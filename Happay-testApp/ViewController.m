//
//  ViewController.m
//  Happay-testApp
//
//  Created by Bratin Mallick on 23/06/15.
//  Copyright (c) 2015 Bratin Mallick. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "Transaction.h"
#import "DataOperationManager.h"
@interface ViewController ()
@property (strong, nonatomic) NSFetchedResultsController *fetchResultsController;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) NSManagedObjectContext *context;
@end

@implementation ViewController
//@synthesize fetchResultsController;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[DataOperationManager sharedInstance] addTransactionListener:self];
    [[DataOperationManager sharedInstance] fetchTransactionData];
    
    _context = [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Transaction" inManagedObjectContext:_context];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timestamp" ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setEntity:entity];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    _fetchResultsController = [[NSFetchedResultsController alloc]
                                              initWithFetchRequest:fetchRequest
                                              managedObjectContext:_context
                                              sectionNameKeyPath:nil
                                              cacheName:@"Viewcontroller_Cache"];
    
    NSError *error;
    BOOL success = [_fetchResultsController performFetch:&error];
    NSLog(@"%d",success);
    _refreshControl = [[UIRefreshControl alloc] init];
    [_refreshControl setAttributedTitle:[[NSAttributedString alloc] initWithString:@"Pull to refresh"]];
    [_refreshControl addTarget:self action:@selector(refreshTableView:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:_refreshControl];
    [self.tableView sendSubviewToBack:_refreshControl];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Tableview delegate & datasource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSLog(@"table load");
    return [[self.fetchResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    if ([[_fetchResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[_fetchResultsController sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    } else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TransactionCell" forIndexPath:indexPath];
    Transaction *transactionObject = [_fetchResultsController objectAtIndexPath:indexPath];
    
    UILabel *remarksLabel= (UILabel*)[cell viewWithTag:1];
    [remarksLabel setText:transactionObject.remarks];

    UILabel *timeLabel= (UILabel*)[cell viewWithTag:3];
    [timeLabel setText:transactionObject.timestamp];
    
    UILabel *transactionIDLabel= (UILabel*)[cell viewWithTag:4];
    [transactionIDLabel setText:transactionObject.transactionID];
    
    UIImageView *profileImageView = (UIImageView *)[cell viewWithTag:2];
    if (transactionObject.profileImage) {
        profileImageView.image = [UIImage imageWithData:transactionObject.profileImage];
        //NSLog(@"local");
    }
    else if (transactionObject.profile)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:transactionObject.profile]];
                transactionObject.profileImage=imageData;
                dispatch_async(dispatch_get_main_queue(), ^{
                    profileImageView.image=[UIImage imageWithData:imageData];
                });
        });
    }
    else
    {
        profileImageView.image=[UIImage imageNamed:@"Not_available"];
    }
    
    return cell;
}

#pragma mark - Transaction Delegate methods

- (void)transactionRequestCompleted
{
    [[DataOperationManager sharedInstance] removeTransactionListener:self];
    NSError *error;
    BOOL success = [_fetchResultsController performFetch:&error];
    [self.tableView reloadData];
    [_refreshControl endRefreshing];
}

#pragma mark - Refesh Control method
-(void) refreshTableView:(id)sender
{
    [[DataOperationManager sharedInstance] addTransactionListener:self];
    [[DataOperationManager sharedInstance] fetchTransactionData];
}
@end
