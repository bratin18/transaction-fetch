//
//  BaseOperation.m
//  Happay-testApp
//
//  Created by Bratin Mallick on 23/06/15.
//  Copyright (c) 2015 Bratin Mallick. All rights reserved.
//

#import "BaseOperation.h"

@implementation BaseOperation
@synthesize parser;
-(BaseResponseParser *) parser
{
    if (!parser) {
        parser = [[BaseResponseParser alloc] init];
//        parser.response= self.response;
    }
    return parser;
}

- (void)main
{
    @autoreleasepool {
        [self submitRequest];  
    }
}

-(void) submitRequest
{
    
}



@end
