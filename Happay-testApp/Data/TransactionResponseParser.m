//
//  TransactionResponseParser.m
//  Happay-testApp
//
//  Created by Bratin Mallick on 23/06/15.
//  Copyright (c) 2015 Bratin Mallick. All rights reserved.
//

#import "TransactionResponseParser.h"
@implementation TransactionResponseParser


-(void)parseResponseData
{
    [super parseResponseData];
}

#pragma mark - NSXMLParserDelegate Methods

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    self.currentString = [NSMutableString string];
    if ([elementName isEqualToString:@"trans"]) {
        
        _currentTransaction = [NSEntityDescription insertNewObjectForEntityForName:@"Transaction" inManagedObjectContext:self.context];
    }
    
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    if ([elementName isEqualToString:@"trans"]) {
        NSFetchRequest *fetchRequest= [[NSFetchRequest alloc] initWithEntityName:@"Transaction"];
        fetchRequest.predicate= [NSPredicate predicateWithFormat:@"transactionID like %@",_currentTransaction.transactionID];
        NSError *error;
        //fetching duplicate entr
        NSArray *array=[self.context executeFetchRequest:fetchRequest error:&error];
        if ([array count]>1) {
            [self.context deleteObject:_currentTransaction];
        }
    }
    else if ([elementName isEqualToString:@"remarks"]) {
        _currentTransaction.remarks=self.currentString;
    }
    else if ([elementName isEqualToString:@"ts"]) {
        _currentTransaction.timestamp=self.currentString;
    }
    else if ([elementName isEqualToString:@"transid"]) {
        _currentTransaction.transactionID=self.currentString;
    }
    else if ([elementName isEqualToString:@"profile"]) {
        _currentTransaction.profile=self.currentString;
    }
    self.currentString=nil;
}
@end
