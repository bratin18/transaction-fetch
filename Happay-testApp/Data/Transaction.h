//
//  Transaction.h
//  Happay-testApp
//
//  Created by Bratin Mallick on 25/06/15.
//  Copyright (c) 2015 Bratin Mallick. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Transaction : NSManagedObject

@property (nonatomic, retain) NSString * profile;
@property (nonatomic, retain) NSString * remarks;
@property (nonatomic, retain) NSString * timestamp;
@property (nonatomic, retain) NSString * transactionID;
@property (nonatomic, retain) NSData * profileImage;

@end
