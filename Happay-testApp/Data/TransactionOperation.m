 //
//  TransactionOperation.m
//  Happay-testApp
//
//  Created by Bratin Mallick on 23/06/15.
//  Copyright (c) 2015 Bratin Mallick. All rights reserved.
//

#import "TransactionOperation.h"
#import "TransactionResponseParser.h"
@implementation TransactionOperation

-(BaseResponseParser *) parser
{
    if (!parser) {
        parser = [[TransactionResponseParser alloc] init];
    }
    return parser;
}

-(void) submitRequest
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://cavac.happay.in/transaction/feed/"]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/xml" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-type"];
    [request setTimeoutInterval:30];
    
    NSError *error = nil;
    NSURLResponse *response = nil;
    NSData *receivedData = nil;
    @try {
        receivedData = [[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error] mutableCopy];
        if (!error) {
            self.parser.responseData = receivedData;
            [self.parser parseResponseData];
            self.parser = nil;
        }
        else
        {
            NSLog(@"error: %@",error);
        }
    }
    @finally {
        receivedData=nil;
    }
    [self.delegate performSelectorOnMainThread:@selector(operationDidComplete:) withObject:self waitUntilDone:YES];
}
@end
