//
//  Transaction.m
//  Happay-testApp
//
//  Created by Bratin Mallick on 25/06/15.
//  Copyright (c) 2015 Bratin Mallick. All rights reserved.
//

#import "Transaction.h"


@implementation Transaction

@dynamic profile;
@dynamic remarks;
@dynamic timestamp;
@dynamic transactionID;
@dynamic profileImage;

@end
