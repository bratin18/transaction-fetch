//
//  DataOperationManager.h
//  Happay-testApp
//
//  Created by Bratin Mallick on 23/06/15.
//  Copyright (c) 2015 Bratin Mallick. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TransactionOperation.h"
#import "RequesterProtocols.h"

@interface DataOperationManager : NSObject <OperationDelegate>{
    TransactionOperation *transationOperation;
    NSMutableSet *transactionListeners;
    BOOL notifTransactionListeners;
}

+(DataOperationManager *) sharedInstance;

-(void) addTransactionListener:(NSObject<OperationsManagerTransactionListener> *) listeners;
-(void) removeTransactionListener:(NSObject<OperationsManagerTransactionListener>  *) listener;
-(void) fetchTransactionData;
@end
