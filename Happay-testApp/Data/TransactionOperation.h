//
//  TransactionOperation.h
//  Happay-testApp
//
//  Created by Bratin Mallick on 23/06/15.
//  Copyright (c) 2015 Bratin Mallick. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseOperation.h"

@interface TransactionOperation : BaseOperation

@end
