//
//  BaseResponseParser.h
//  Happay-testApp
//
//  Created by Bratin Mallick on 23/06/15.
//  Copyright (c) 2015 Bratin Mallick. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface BaseResponseParser : NSObject <NSXMLParserDelegate>
{
    NSDictionary *xmlDictionary;
    NSData *responseData;
    NSMutableString *currentString;
    NSManagedObjectContext *context;
}
@property (nonatomic, strong) NSData *responseData;
@property (nonatomic, strong) NSMutableString *currentString;
@property (nonatomic, strong) NSManagedObjectContext *context;
-(void) parseResponseData;
@end
