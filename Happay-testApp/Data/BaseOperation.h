//
//  BaseOperation.h
//  Happay-testApp
//
//  Created by Bratin Mallick on 23/06/15.
//  Copyright (c) 2015 Bratin Mallick. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OperationDelegateProtocol.h"
#import "BaseResponseParser.h"
@interface BaseOperation : NSOperation
{
    NSObject<OperationDelegate> *delegate;
    BaseResponseParser *parser;
}

@property (nonatomic, strong) NSObject<OperationDelegate> *delegate;
@property (nonatomic, strong) BaseResponseParser *parser;
-(void) submitRequest;
@end
