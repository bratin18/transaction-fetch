//
//  TransactionResponseParser.h
//  Happay-testApp
//
//  Created by Bratin Mallick on 23/06/15.
//  Copyright (c) 2015 Bratin Mallick. All rights reserved.
//

#import "BaseResponseParser.h"
#import "Transaction.h"
@interface TransactionResponseParser : BaseResponseParser
@property (nonatomic, strong) Transaction *currentTransaction;
@end
