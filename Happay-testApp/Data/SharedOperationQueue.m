//
//  SharedOperationQueue.m
//  Happay-testApp
//
//  Created by Bratin Mallick on 23/06/15.
//  Copyright (c) 2015 Bratin Mallick. All rights reserved.
//

#import "SharedOperationQueue.h"

@implementation SharedOperationQueue
//Creating singleton instance of Operation Queue to handle the operation pipeline
+(SharedOperationQueue *) sharedInstance
{
    static SharedOperationQueue *sharedQueue = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedQueue=[[self alloc] init];
    });
    return sharedQueue;
}
@end
