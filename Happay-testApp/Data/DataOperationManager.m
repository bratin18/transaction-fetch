//
//  DataOperationManager.m
//  Happay-testApp
//
//  Created by Bratin Mallick on 23/06/15.
//  Copyright (c) 2015 Bratin Mallick. All rights reserved.
//

#import "DataOperationManager.h"
#import "SharedOperationQueue.h"

@interface DataOperationManager()
@property (strong,nonatomic) NSMutableSet *transactionListeners;
-(void) notifyTransactionListeners;
-(BOOL) transactionRequestInProgress;
@end

@implementation DataOperationManager

- (id)init
{
    self = [super init];
    if (self) {
        _transactionListeners=[[NSMutableSet alloc] init];
    }
    return self;
}

+(DataOperationManager *) sharedInstance
{
    static DataOperationManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager=[[self alloc] init];
    });
    return sharedManager;
}

-(BOOL) transactionRequestInProgress
{
    return (nil != transationOperation);
}


-(void) addTransactionListener:(NSObject<OperationsManagerTransactionListener> *) listeners
{
    [_transactionListeners addObject:listeners];
}
-(void) removeTransactionListener:(NSObject *) listener
{
    [_transactionListeners removeObject:listener];
    transationOperation=nil;
}

-(void) fetchTransactionData
{
    if (!self.transactionRequestInProgress) {
        transationOperation = [[TransactionOperation alloc] init];
        transationOperation.delegate= self;
        [[SharedOperationQueue sharedInstance] addOperation:transationOperation];
        
    }
}

-(void) notifyTransactionListeners
{
    notifTransactionListeners=YES;
    [_transactionListeners makeObjectsPerformSelector:@selector(transactionRequestCompleted)];
    notifTransactionListeners=NO;
}

#pragma mark - Operation Delegate
- (void)operationDidComplete:(BaseOperation *)operation
{
    if ([operation isKindOfClass:[TransactionOperation class]]) {
        [self notifyTransactionListeners];
    }
}

@end
