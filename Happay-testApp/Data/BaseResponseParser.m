//
//  BaseResponseParser.m
//  Happay-testApp
//
//  Created by Bratin Mallick on 23/06/15.
//  Copyright (c) 2015 Bratin Mallick. All rights reserved.
//

#import "BaseResponseParser.h"

@implementation BaseResponseParser

- (id)init
{
    self = [super init];
    if (self) {
        //getting instance of Managed Object Context
        _context=[(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    }
    return self;
}


-(void) parseResponseData
{
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:_responseData];
    [parser setDelegate:self];
    [parser parse];
}                                       

#pragma mark - NSXMLParserDelegate Methods

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    if ([elementName isEqualToString:@"trans"]) {
        
    }
    
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    if ([elementName isEqualToString:@"trans"]) {
        
    }
    
}
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    [_currentString appendString:string];
}
@end
