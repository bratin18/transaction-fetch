//
//  OperationDelegateProtocol.h
//  Happay-testApp
//
//  Created by Bratin Mallick on 6/23/15.
//  Copyright (c) 2015 Bratin Mallick. All rights reserved.
//

@class BaseOperation;
@protocol OperationDelegate
- (void)operationDidComplete:(BaseOperation *)operation;
@end